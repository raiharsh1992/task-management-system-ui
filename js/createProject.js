var selectedEmp = 0;
$(document).ready(function() {
  var datePicker = document.getElementById("expStartDate");
  var today = new Date();
  var options = {
    autoClose:true,
    format:'dd/mm/yyyy',
    minDate:today
  }
  var instances = M.Datepicker.init(datePicker, options);
  displayEmployees();
});

$(document).on("change", '#manageEmployees', function (event) {
  selectedEmp = parseInt($(this).children(":selected").attr("value"));
  console.log(selectedEmp);
});

function displayEmployees() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewteam/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      var displaySize = 0;
      var tableView = ''
      $.each(usingData, function(index, value) {
        var imgSrc = 'img/cross.png';
        if (value.isManager){
          imgSrc = 'img/tick.png';
        }
        tableView = tableView+'<option value="'+value.clientId+'">'+value.clientName+' '+value.clientNumber+'</option>'
      });
      $('#manageEmployees').append(tableView);
      $('select')
        .not('.disabled')
        .formSelect();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function createNewProject() {
  event.preventDefault();
  var projectName = document.getElementById("projectName").value;
  var expStartDate = document.getElementById("expStartDate").value;
  var description = document.getElementById("description").value;
  var expDuration = document.getElementById("expDuration").value;
  if (projectName != "") {
    if (expStartDate != "") {
      if (description != "") {
        if (expDuration != "") {
          if (selectedEmp >0) {
            var myObj = {};
            console.log(clientName);
            myObj["assignedTo"] = selectedEmp;
            myObj["expectedStartDate"] = expStartDate;
            myObj["description"] = description;
            myObj["name"] = projectName;
            myObj["expectedDuration"] = parseInt(expDuration);
            myObj['userId'] = sessionInfo.sessionInfo.userId;
            var jsonObj = JSON.stringify(myObj);
            $.ajax({
              url: beUrl() + 'createproject/',
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: 'application/json',
              data: jsonObj,
              headers: {
                'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
                'sessionKey':sessionInfo.sessionInfo.sessionKey
              },
              success: function(response) {
                M.toast({
                  html: 'New project created'
                });
                location.assign("index.html");
              },
              error: function(response) {
                var usingInfo = JSON.parse(JSON.stringify(response));
                M.toast({
                  html: usingInfo.responseJSON.Data
                });
                if (usingInfo.status == 401) {
                  logoutModule();
                }
              }
            });
          } else {
            M.toast({
              html: 'Kindly select a team member to assign'
            });
          }
        } else {
          M.toast({
            html: 'Expected duration should be greater than 0'
          });
        }
      } else {
        M.toast({
          html: 'Description cannot be left blank'
        });
      }
    } else {
      M.toast({
        html: 'Project start date cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Project name cannot be left blank'
    });
  }
}
