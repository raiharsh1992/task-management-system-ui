var sessionInfo;
var isDirect = 0;
var createEmployee = '<li class="bold hoverable"><a href="createuser.html" class="waves-effect waves-teal inLineText"><img src="img/createuser.png" class="menuManagerIcon" /><span class="fontSize">Create User</span></a></li>';
var viewEmployee = '<li class="bold hoverable"><a href="users.html" class="waves-effect waves-teal inLineText"><img src="img/movie.png" class="menuManagerIcon" /><span class="fontSize">View Users</span></a></li>';
var viewProject = '<li class="bold hoverable"><a href="projects.html" class="waves-effect waves-teal inLineText"><img src="img/viewproject.png" class="menuManagerIcon" /><span class="fontSize">View Projects</span></a></li>';
var createProject = '<li class="bold hoverable"><a href="createproject.html" class="waves-effect waves-teal inLineText"><img src="img/createproject.png" class="menuManagerIcon" /><span class="fontSize">Create Project</span></a></li>';
var logOut = '<li class="bold hoverable"><a href="javascript:logoutModule()" class="waves-effect waves-teal inLineText"><img src="img/logout.png" class="menuManagerIcon" /><span class="fontSize">Logout</span></a></li>';

$(document).ready(function() {
  if (localStorage.getItem("adminRememberMe")) {
    var rememberMe = parseInt(localStorage.getItem("adminRememberMe"));
    if (rememberMe == 1) {
      if (localStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(localStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    } else {
      if (sessionStorage.getItem("adminSessionInfo")) {
        sessionInfo = JSON.parse(sessionStorage.getItem("adminSessionInfo"));
      } else {
        location.replace("login.html");
      }
    }
  } else {
    location.replace("login.html");
  }
  menuManager();

});

function menuManager() {
  var menuPrint = '';
  menuPrint = menuPrint + viewEmployee;
  if (sessionInfo.sessionInfo.isModerator==1){
    menuPrint = menuPrint + createEmployee + createProject;
  }
  menuPrint = menuPrint + viewProject;
  menuPrint = menuPrint + logOut;
  $("#nav-mobile").append(menuPrint);
}

function logoutModule() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "logout/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: 'User logged out successfully'
      });
      localStorage.removeItem("adminSessionInfo");
      sessionStorage.removeItem("adminSessionInfo");
      localStorage.removeItem("adminRememberMe");
      location.replace("login.html");
    },
    error: function(response) {
      M.toast({
        html: 'Issue while logging out user'
      });
      localStorage.removeItem("adminSessionInfo");
      sessionStorage.removeItem("adminSessionInfo");
      localStorage.removeItem("adminRememberMe");
      location.replace("login.html");
    }
  });

}

function getUrlVars() {

  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
    vars[key] = value;

  });
  return vars;
}
