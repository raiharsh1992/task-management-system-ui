var currentPage = 1;
var currentPageSize = 100;
$(document).ready(function() {
  displayProjectList();
});

function displayProjectList() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['requestFor'] = "PROJ";
  myObj['pageSize'] = currentPageSize;
  myObj['pageCount'] = currentPage;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewlist/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      var displaySize = 0;
      var tableView = ''
      $.each(usingData, function(index, value) {
        tableView = tableView+'<tr class="hoverable"><td>'+value.projectName+'</td><td>'+value.desc+'</td><td>'+value.currentStatus+'</td><td>'+value.creatorName+'</td><td>'+value.assignorName+'</td><td><a href="projectdetails.html?projectId='+value.projectId+'">Details</a></td></tr>'
        displaySize = 1;
      });
      if (displaySize==1){
        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = tableView;
        var maxDisplayed = currentPageSize*currentPage;
        if(usingInfo.totalCount>maxDisplayed){
          document.getElementById("nextButton").style.display = "inline-block";
        }
        else{
          document.getElementById("nextButton").style.display = "none";
        }
        if(parseInt(currentPage)==1){
          document.getElementById("prevButton").style.display = "none";
        }
        else{
          document.getElementById("prevButton").style.display = "inline-block";
        }
      }
      else{
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
        document.getElementById("tableContainer").innerHTML = "";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });

}

function goToNext() {
  currentPage = currentPage+1;
  displayProjectList();
}

function goToPrevious() {
  if (currentPage>1){
      currentPage = currentPage-1;
      displayProjectList();
  }
}
