
$(document).ready(function() {

  $(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
      document.getElementById("password-field1").innerHTML = "lock_open";
    } else {
      input.attr("type", "password");
      document.getElementById("password-field1").innerHTML = "lock_outline";
    }
  });
  $('#icon_prefix').on('focusout', function() {
    checkUserNameAvailablity();
  });
  $('#mobileNumber').on('focusout', function() {
    checkUserNumberAvailablity();
  });
  $('#email').on('focusout', function() {
    checkUserEmailAvailablity();
  });
  $('#password-field').on('focusout', function() {
    checkUserPasswordAvailablity();
  });
});

function checkUserNumberAvailablity() {
  if (document.getElementById("mobileNumber").value != "") {
    var myObj = {};
    myObj["checkValue"] = document.getElementById("mobileNumber").value;
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['checkingFor'] = "NUMBER";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'checkunique/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey':sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Unique Phone number passed'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: 'Phone number field cannot be left blank'
    });
  }
}

function checkUserEmailAvailablity() {
  if (document.getElementById("email").value != "") {
    var myObj = {};
    myObj["checkValue"] = document.getElementById("email").value;
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['checkingFor'] = "EMAIL";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'checkunique/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey':sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Unique email id passed'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: 'Email field cannot be left blank'
    });
  }
}

function checkUserPasswordAvailablity() {
  if (document.getElementById("password-field").value != "") {
    var myObj = {};
    myObj["checkValue"] = document.getElementById("password-field").value;
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['checkingFor'] = "PASS";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'checkunique/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey':sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Valid password passed'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: 'Password field cannot be left blank'
    });
  }
}


function checkUserNameAvailablity() {
  if (document.getElementById("icon_prefix").value != "") {
    var myObj = {};
    myObj["checkValue"] = document.getElementById("icon_prefix").value;
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['checkingFor'] = "UNAME";
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + 'checkunique/',
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey':sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Username available'
        });
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: 'Username cannot be left blank'
    });
  }
}

function createUser() {
  event.preventDefault();
  var userName = document.getElementById("icon_prefix").value;
  var password = document.getElementById("password-field").value;
  var clientName = document.getElementById("clientNameWork").value;
  var number = document.getElementById("mobileNumber").value;
  var email = document.getElementById("email").value;
  var rememberMe = document.getElementById("isModerator").checked;
  if (userName != "") {
    if (password != "") {
      if (clientName != "") {
        if (number != "") {
          if (email != "") {
            var myObj = {};
            console.log(clientName);
            myObj["userName"] = userName;
            myObj["password"] = password;
            myObj["number"] = number;
            myObj["adminName"] = clientName;
            myObj["email"] = email;
            myObj['userId'] = sessionInfo.sessionInfo.userId;
            if(rememberMe==true){
              myObj['isManager']=1;
            }
            else{
              myObj['isManager']=0;
            }
            var jsonObj = JSON.stringify(myObj);
            $.ajax({
              url: beUrl() + 'createuser/',
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: 'application/json',
              data: jsonObj,
              headers: {
                'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
                'sessionKey':sessionInfo.sessionInfo.sessionKey
              },
              success: function(response) {
                M.toast({
                  html: 'New user created'
                });
                location.assign("index.html");
              },
              error: function(response) {
                var usingInfo = JSON.parse(JSON.stringify(response));
                M.toast({
                  html: usingInfo.responseJSON.Data
                });
                if (usingInfo.status == 401) {
                  logoutModule();
                }
              }
            });
          } else {
            M.toast({
              html: 'Email cannot be left blank'
            });
          }
        } else {
          M.toast({
            html: 'Phone number cannot be left blank'
          });
        }
      } else {
        M.toast({
          html: 'Name cannot be left blank'
        });
      }
    } else {
      M.toast({
        html: 'Password cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Username cannot be left blank'
    });
  }
}
