var currentPage = 1;
var currentPageSize = 100;
var projectId = 0,
  selectedEmp = 0,
  selectedEmp1 = 0;
var usingData, editModalIns, uploadModalIns, assigneeModalIns, newTaskModalIns;
$(document).ready(function() {
  projectId = getUrlVars()["projectId"];
  var datePicker = document.getElementById("expStartDate1");
  var today = new Date();
  var options = {
    autoClose: true,
    format: 'dd/mm/yyyy',
    minDate: today,
    container: 'body'
  }
  var instances = M.Datepicker.init(datePicker, options);
  displayEmployees();
  displayProjectList();
  populateDetails();
  displayEmployees();
  var optionsModal = {
    dismissible: true,
    preventScrolling: false
  };
  var elemEdit = document.getElementById("editInfoModal");
  var elemUpload = document.getElementById("uploadImageModal")
  var elemAssignee = document.getElementById("changeAssigneeModal")
  var elemNewTask = document.getElementById("changeNewTaskModal")
  editModalIns = M.Modal.init(elemEdit, optionsModal);
  uploadModalIns = M.Modal.init(elemUpload, optionsModal);
  assigneeModalIns = M.Modal.init(elemAssignee, optionsModal);
  newTaskModalIns = M.Modal.init(elemNewTask, optionsModal);

});

$(document).on("change", '#manageEmployees', function(event) {
  selectedEmp = parseInt($(this).children(":selected").attr("value"));
  console.log(selectedEmp);
});

$(document).on("change", '#manageEmployees1', function(event) {
  selectedEmp1 = parseInt($(this).children(":selected").attr("value"));
  console.log(selectedEmp);
});

function displayEmployees() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewteam/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      var displaySize = 0;
      var tableView = ''
      $.each(usingData, function(index, value) {
        var imgSrc = 'img/cross.png';
        if (value.isManager) {
          imgSrc = 'img/tick.png';
        }
        tableView = tableView + '<option value="' + value.clientId + '">' + value.clientName + ' ' + value.clientNumber + '</option>'
      });
      $('#manageEmployees').append(tableView);
      $('#manageEmployees1').append(tableView);
      $('select')
        .not('.disabled')
        .formSelect();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function populateDetails() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['requestFor'] = "PROJ";
  myObj['requestId'] = projectId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewdetail/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      usingData = usingInfo.Data;
      document.getElementById("headName").innerHTML = usingData.projectName;
      document.getElementById("descriptionText").innerHTML = usingData.description;
      document.getElementById("cratedOn").innerHTML = usingData.createdOn;
      document.getElementById("expStart").innerHTML = usingData.expectedStartDate;
      document.getElementById("expEnd").innerHTML = usingData.expectedEndDate;
      document.getElementById("expDuration").innerHTML = usingData.expectedDuration;
      document.getElementById("lastEdit").innerHTML = usingData.lastEditDate;
      document.getElementById("actualStart").innerHTML = usingData.actualStartDate;
      document.getElementById("actualEnd").innerHTML = usingData.actualEndDate;
      document.getElementById("actualDuration").innerHTML = usingData.actualDuration;
      document.getElementById("createdBy").innerHTML = usingData.creatorName;
      document.getElementById("currentStatus").innerHTML = usingData.currentStatus;
      document.getElementById("assignedTo").innerHTML = usingData.assignedName;
      manageButtonsClickAvail();
      if (usingData.imageUrl != null) {
        document.getElementById("projectImage").src = beUrl() + "media/" + usingData.imageUrl;
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function manageButtonsClickAvail() {
  var isModerator = sessionInfo.sessionInfo.isModerator;
  var currentStatus = usingData.currentStatus;
  var isAssignedToMe = usingData.isAssignedToMe;
  if (!((isAssignedToMe == 1) && ((currentStatus == "NEW") || (currentStatus == "START")))) {
    document.getElementById("updateStatusButton").classList.add("disabled");
  }
  if (!(((isModerator == 1) || (isAssignedToMe == 1)) && (currentStatus == "NEW"))) {
    document.getElementById("assigneeButton").classList.add("disabled");
  }
  if (!((isModerator == 1)&&((currentStatus=="NEW")||(currentStatus=="START")))) {
    document.getElementById("createTaskButton").classList.add("disabled");
  }
  if (!((isModerator == 1) || (isAssignedToMe == 1))) {
    document.getElementById("editButton").classList.add("disabled");
    document.getElementById("uploadImageButton").classList.add("disabled");
  }
}

function displayProjectList() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['requestFor'] = "TASK";
  myObj['pageSize'] = currentPageSize;
  myObj['pageCount'] = currentPage;
  myObj['projectId'] = projectId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewlist/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      var displaySize = 0;
      var tableView = ''
      $.each(usingData, function(index, value) {
        var deleteInfo = "--NA--";
        if ((value.currentStatus == "NEW") && (sessionInfo.sessionInfo.isModerator == 1)) {
          deleteInfo = '<a href="javascript:initiateDelete('+value.taskId+')"><img class="tableImage" src="img/delete.png"></a>'
        }
        tableView = tableView + '<tr class="hoverable"><td>' + value.taskName + '</td><td>' + value.desc + '</td><td>' + value.currentStatus + '</td><td>' + value.creatorName + '</td><td>' + value.assignorName + '</td><td>' + deleteInfo + '</td><td><a href="taskdetails.html?taskId=' + value.taskId + '">Details</a></td></tr>'
        displaySize = 1;
      });
      if (displaySize == 1) {
        document.getElementById("displayNothing").style.display = "none";
        document.getElementById("tableContainer").innerHTML = tableView;
        var maxDisplayed = currentPageSize * currentPage;
        if (usingInfo.totalCount > maxDisplayed) {
          document.getElementById("nextButton").style.display = "inline-block";
        } else {
          document.getElementById("nextButton").style.display = "none";
        }
        if (parseInt(currentPage) == 1) {
          document.getElementById("prevButton").style.display = "none";
        } else {
          document.getElementById("prevButton").style.display = "inline-block";
        }
      } else {
        document.getElementById("prevButton").style.display = "none";
        document.getElementById("nextButton").style.display = "none";
        document.getElementById("displayNothing").style.display = "inline-block";
        document.getElementById("tableContainer").innerHTML = "";
      }
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });

}

function goToNext() {
  currentPage = currentPage + 1;
  displayProjectList();
}

function goToPrevious() {
  if (currentPage > 1) {
    currentPage = currentPage - 1;
    displayProjectList();
  }
}

function editOpenModal() {
  document.getElementById('projectName').value = usingData.projectName;
  document.getElementById('description').value = usingData.description;
  M.updateTextFields();
  editModalIns.open();
}

function closeEditFunction() {
  editModalIns.close();
}

function closeUploadFunction() {
  uploadModalIns.close();
}

function uploadOpenModal() {
  uploadModalIns.open();
}

function assigneeOpenModal() {
  assigneeModalIns.open();
}

function closeAssigneeFunction() {
  assigneeModalIns.close();
}

function newTaskOpenModal() {
  newTaskModalIns.open();
}

function closeNewTaskFunction() {
  newTaskModalIns.close();
}

function completeUploadFunction() {
  var form = new FormData();
  form.append("userId", sessionInfo.sessionInfo.userId);
  form.append("projectId", projectId);
  form.append("profileImage", $('#uploadImageField')[0].files[0]);
  var settings = {
    "url": beUrl() + "uploadimage/",
    "method": "POST",
    "timeout": 0,
    "headers": {
      "basicAuthenticate": sessionInfo.sessionInfo.basicAuth,
      "sessionKey": sessionInfo.sessionInfo.sessionKey
    },
    "processData": false,
    "mimeType": "multipart/form-data",
    "contentType": false,
    "data": form
  };
  $.ajax(settings).done(function(response) {
    location.reload();
  });
}

function completeAssigneeFunction() {
  if (selectedEmp != 0) {
    var myObj = {};
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['assignmentFor'] = "PROJ";
    myObj['assignmentId'] = projectId;
    myObj['assignedTo'] = parseInt(selectedEmp);
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + "assigntask/",
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey': sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Assignment updated'
        });
        location.reload();
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: 'Project name cannot be left blank'
    });
  }
}

function completeEditFunction() {
  var projectName = document.getElementById('projectName').value;
  var description = document.getElementById('description').value;
  if (projectName != "") {
    if (description != "") {
      var myObj = {};
      myObj['userId'] = sessionInfo.sessionInfo.userId;
      myObj['editType'] = "PROJ";
      myObj['editId'] = projectId;
      myObj['name'] = projectName;
      myObj['description'] = description;
      var jsonObj = JSON.stringify(myObj);
      $.ajax({
        url: beUrl() + "editwork/",
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: jsonObj,
        headers: {
          'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
          'sessionKey': sessionInfo.sessionInfo.sessionKey
        },
        success: function(response) {
          M.toast({
            html: 'Project edited'
          });
          location.reload();
        },
        error: function(response) {
          var usingInfo = JSON.parse(JSON.stringify(response));
          M.toast({
            html: usingInfo.responseJSON.Data
          });
          if (usingInfo.status == 401) {
            logoutModule();
          }
        }
      });
    } else {
      M.toast({
        html: 'Description cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Project name cannot be left blank'
    });
  }
}

function initiateDelete(taskId) {
  if (window.confirm('You sure want to delete the task?')) {
    deleteTheTask(taskId)
  }
}

function initiateStatusChange() {
  if (window.confirm('You sure want to change the status?')) {
    changeTheStatus()
  }
}

function changeTheStatus() {
  var myObj = {};
  var newStatus = "START";
  if(usingData.currentStatus=="START"){
    newStatus = "END"
  }
  myObj["updateStaus"] = newStatus;
  myObj["updateType"] = "PROJ";
  myObj["updateId"] = projectId;
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + 'updatework/',
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: 'Project status updated'
      });
      location.reload()
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function deleteTheTask(taskId) {
  var myObj = {};
  myObj["deletionId"] = taskId;
  myObj["deletionFor"] = "TASK";
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + 'deletework/',
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: 'Task deleted'
      });
      currentPage = 1;
      displayProjectList();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function completeNewFunction() {
  var projectName = document.getElementById("projectName1").value;
  var expStartDate = document.getElementById("expStartDate1").value;
  var description = document.getElementById("description1").value;
  var expDuration = document.getElementById("expDuration1").value;
  if (projectName != "") {
    if (expStartDate != "") {
      if (description != "") {
        if (expDuration != "") {
          if (selectedEmp1 > 0) {
            var myObj = {};
            myObj["assignedTo"] = selectedEmp1;
            myObj["expectedStartDate"] = expStartDate;
            myObj["description"] = description;
            myObj["name"] = projectName;
            myObj["expectedDuration"] = parseInt(expDuration);
            myObj["projectId"] = projectId;
            myObj['userId'] = sessionInfo.sessionInfo.userId;
            var jsonObj = JSON.stringify(myObj);
            $.ajax({
              url: beUrl() + 'createtask/',
              type: 'POST',
              dataType: 'json',
              processData: false,
              contentType: 'application/json',
              data: jsonObj,
              headers: {
                'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
                'sessionKey': sessionInfo.sessionInfo.sessionKey
              },
              success: function(response) {
                M.toast({
                  html: 'New task created'
                });
                //location.assign("index.html");
                currentPage = 1;
                displayProjectList();
                newTaskModalIns.close();
              },
              error: function(response) {
                var usingInfo = JSON.parse(JSON.stringify(response));
                M.toast({
                  html: usingInfo.responseJSON.Data
                });
                if (usingInfo.status == 401) {
                  logoutModule();
                }
              }
            });
          } else {
            M.toast({
              html: 'Kindly select a team member to assign'
            });
          }
        } else {
          M.toast({
            html: 'Expected duration should be greater than 0'
          });
        }
      } else {
        M.toast({
          html: 'Description cannot be left blank'
        });
      }
    } else {
      M.toast({
        html: 'Task start date cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Task name cannot be left blank'
    });
  }
}
