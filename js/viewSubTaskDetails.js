var projectId = 0,
  selectedEmp = 0;
var usingData, editModalIns,  assigneeModalIns, newTaskModalIns;
$(document).ready(function() {
  projectId = getUrlVars()["subTaskId"];
  var datePicker = document.getElementById("expStartDate1");
  var today = new Date();
  var options = {
    autoClose: true,
    format: 'dd/mm/yyyy',
    minDate: today,
    container: 'body'
  }
  var instances = M.Datepicker.init(datePicker, options);
  displayEmployees();
  populateDetails();
  var optionsModal = {
    dismissible: true,
    preventScrolling: false
  };
  var elemEdit = document.getElementById("editInfoModal");
  var elemAssignee = document.getElementById("changeAssigneeModal")
  editModalIns = M.Modal.init(elemEdit, optionsModal);
  assigneeModalIns = M.Modal.init(elemAssignee, optionsModal);

});

$(document).on("change", '#manageEmployees', function(event) {
  selectedEmp = parseInt($(this).children(":selected").attr("value"));
  console.log(selectedEmp);
});

function displayEmployees() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewteam/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      var displaySize = 0;
      var tableView = ''
      $.each(usingData, function(index, value) {
        var imgSrc = 'img/cross.png';
        if (value.isManager) {
          imgSrc = 'img/tick.png';
        }
        tableView = tableView + '<option value="' + value.clientId + '">' + value.clientName + ' ' + value.clientNumber + '</option>'
      });
      $('#manageEmployees').append(tableView);
      $('#manageEmployees1').append(tableView);
      $('select')
        .not('.disabled')
        .formSelect();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function populateDetails() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  myObj['requestFor'] = "SUBTASK";
  myObj['requestId'] = projectId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewdetail/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      usingData = usingInfo.Data;
      document.getElementById("headName").innerHTML = usingData.taskName;
      document.getElementById("descriptionText").innerHTML = usingData.description;
      document.getElementById("cratedOn").innerHTML = usingData.createdOn;
      document.getElementById("expStart").innerHTML = usingData.expectedStartDate;
      document.getElementById("expEnd").innerHTML = usingData.expectedEndDate;
      document.getElementById("expDuration").innerHTML = usingData.expectedDuration;
      document.getElementById("lastEdit").innerHTML = usingData.lastEditDate;
      document.getElementById("actualStart").innerHTML = usingData.actualStartDate;
      document.getElementById("actualEnd").innerHTML = usingData.actualEndDate;
      document.getElementById("actualDuration").innerHTML = usingData.actualDuration;
      document.getElementById("createdBy").innerHTML = usingData.creatorName;
      document.getElementById("currentStatus").innerHTML = usingData.currentStatus;
      document.getElementById("assignedTo").innerHTML = usingData.assignedName;
      manageButtonsClickAvail();
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}

function manageButtonsClickAvail() {
  var isModerator = sessionInfo.sessionInfo.isModerator;
  var currentStatus = usingData.currentStatus;
  var isAssignedToMe = usingData.isAssignedToMe;
  if (!((isAssignedToMe == 1) && ((currentStatus == "NEW") || (currentStatus == "START")))) {
    document.getElementById("updateStatusButton").classList.add("disabled");
  }
  if (!(((isModerator == 1) || (isAssignedToMe == 1)) && (currentStatus == "NEW"))) {
    document.getElementById("assigneeButton").classList.add("disabled");
  }
  if (!((isModerator == 1) || (isAssignedToMe == 1))) {
    document.getElementById("editButton").classList.add("disabled");
  }
}

function editOpenModal() {
  document.getElementById('projectName').value = usingData.taskName;
  document.getElementById('description').value = usingData.description;
  M.updateTextFields();
  editModalIns.open();
}

function closeEditFunction() {
  editModalIns.close();
}

function assigneeOpenModal() {
  assigneeModalIns.open();
}

function closeAssigneeFunction() {
  assigneeModalIns.close();
}

function completeAssigneeFunction() {
  if (selectedEmp != 0) {
    var myObj = {};
    myObj['userId'] = sessionInfo.sessionInfo.userId;
    myObj['assignmentFor'] = "SUBTASK";
    myObj['assignmentId'] = projectId;
    myObj['assignedTo'] = parseInt(selectedEmp);
    var jsonObj = JSON.stringify(myObj);
    $.ajax({
      url: beUrl() + "assigntask/",
      type: 'POST',
      dataType: 'json',
      processData: false,
      contentType: 'application/json',
      data: jsonObj,
      headers: {
        'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
        'sessionKey': sessionInfo.sessionInfo.sessionKey
      },
      success: function(response) {
        M.toast({
          html: 'Assignment updated'
        });
        location.reload();
      },
      error: function(response) {
        var usingInfo = JSON.parse(JSON.stringify(response));
        M.toast({
          html: usingInfo.responseJSON.Data
        });
        if (usingInfo.status == 401) {
          logoutModule();
        }
      }
    });
  } else {
    M.toast({
      html: 'Project name cannot be left blank'
    });
  }
}

function completeEditFunction() {
  var projectName = document.getElementById('projectName').value;
  var description = document.getElementById('description').value;
  if (projectName != "") {
    if (description != "") {
      var myObj = {};
      myObj['userId'] = sessionInfo.sessionInfo.userId;
      myObj['editType'] = "SUBTASK";
      myObj['editId'] = projectId;
      myObj['name'] = projectName;
      myObj['description'] = description;
      var jsonObj = JSON.stringify(myObj);
      $.ajax({
        url: beUrl() + "editwork/",
        type: 'POST',
        dataType: 'json',
        processData: false,
        contentType: 'application/json',
        data: jsonObj,
        headers: {
          'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
          'sessionKey': sessionInfo.sessionInfo.sessionKey
        },
        success: function(response) {
          M.toast({
            html: 'Task edited'
          });
          location.reload();
        },
        error: function(response) {
          var usingInfo = JSON.parse(JSON.stringify(response));
          M.toast({
            html: usingInfo.responseJSON.Data
          });
          if (usingInfo.status == 401) {
            logoutModule();
          }
        }
      });
    } else {
      M.toast({
        html: 'Description cannot be left blank'
      });
    }
  } else {
    M.toast({
      html: 'Task name cannot be left blank'
    });
  }
}

function initiateStatusChange() {
  if (window.confirm('You sure want to change the status?')) {
    changeTheStatus()
  }
}

function changeTheStatus() {
  var myObj = {};
  var newStatus = "START";
  if(usingData.currentStatus=="START"){
    newStatus = "END"
  }
  myObj["updateStaus"] = newStatus;
  myObj["updateType"] = "SUBTASK";
  myObj["updateId"] = projectId;
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + 'updatework/',
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey': sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      M.toast({
        html: 'Project status updated'
      });
      location.reload()
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });
}
