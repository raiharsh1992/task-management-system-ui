$(document).ready(function() {
  displayEmployees();
});

function displayEmployees() {
  var myObj = {};
  myObj['userId'] = sessionInfo.sessionInfo.userId;
  var jsonObj = JSON.stringify(myObj);
  $.ajax({
    url: beUrl() + "viewteam/",
    type: 'POST',
    dataType: 'json',
    processData: false,
    contentType: 'application/json',
    data: jsonObj,
    headers: {
      'basicAuthenticate': sessionInfo.sessionInfo.basicAuth,
      'sessionKey':sessionInfo.sessionInfo.sessionKey
    },
    success: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      var usingData = usingInfo.Data;
      var displaySize = 0;
      var tableView = ''
      $.each(usingData, function(index, value) {
        var imgSrc = 'img/cross.png';
        if (value.isManager){
          imgSrc = 'img/tick.png';
        }
        tableView = tableView+'<tr class="hoverable"><td>'+value.clientName+'</td><td>'+value.clientNumber+'</td><td>'+value.clientEmail+'</td><td><img class="tableImage" src="'+imgSrc+'"></td></tr>'
      });
      document.getElementById("displayNothing").style.display = "none";
      document.getElementById("tableContainer").innerHTML = tableView;
    },
    error: function(response) {
      var usingInfo = JSON.parse(JSON.stringify(response));
      M.toast({
        html: usingInfo.responseJSON.Data
      });
      if (usingInfo.status == 401) {
        logoutModule();
      }
    }
  });

}
